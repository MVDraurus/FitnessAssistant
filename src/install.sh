#!/bin/bash

if [ "$(id -u)" != "0" ]
then
        echo "Установка требует прав супер пользователя!"
        exit 0;
fi

cp -r ../../FitnessAssistant /usr/share/ || cp -r ../../FitnessAssistant-master /usr/share/ 
mkdir -p /usr/share/FitnessAssistant/usr/
chmod 777 /usr/share/FitnessAssistant/usr/

touch /usr/bin/fitness-assistant
chmod 777 /usr/bin/fitness-assistant
echo "#!/bin/bash">/usr/bin/fitness-assistant
echo "cd /usr/share/FitnessAssistant/src/ && export LD_LIBRARY_PATH=/usr/share/FitnessAssistant/src/SharedLib && exec /usr/share/FitnessAssistant/src/main.out">>/usr/bin/fitness-assistant

touch /usr/share/applications/'Fitness Assistant'.desktop
echo "[Desktop Entry]"> /usr/share/applications/'Fitness Assistant'.desktop
echo "version=1.0">> /usr/share/applications/'Fitness Assistant'.desktop
echo "Type=Application">> /usr/share/applications/'Fitness Assistant'.desktop
echo "Name=Fitness Assistant">> /usr/share/applications/'Fitness Assistant'.desktop
echo "Exec=fitness-assistant">> /usr/share/applications/'Fitness Assistant'.desktop
echo "Path=/usr/share/FitnessAssistant/usr">> /usr/share/applications/'Fitness Assistant'.desktop
echo "Icon=/usr/share/pixmaps/fitness-assistant.png" >> /usr/share/applications/'Fitness Assistant'.desktop
chmod 777 /usr/share/applications/'Fitness Assistant'.desktop
cp Image/fitness-assistant.png /usr/share/pixmaps/fitness-assistant.png
echo "Программа успешно установленна на ваш ПК!"
