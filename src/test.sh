#!/bin/bash

if [ "$(id -u)" != "0" ];then
	echo "Установка требует прав супер пользователя!"
	exit 0;
fi

[[ -z "$(dpkg-query -W --showformat='${Status}\n' libgtkmm-3.0-dev |grep "install ok installed")" ]] && apt-get install libgtkmm-3.0-dev
[[ -z "$(dpkg-query -W --showformat='${Status}\n' g++ |grep "install ok installed")" ]] && apt-get install g++

echo "Все зависимости установлены!"
