#ifndef GTKMM_CALCULDATA_HPP
#define GTKMM_CALCULDATA_HPP

#include "FitnessAssistant.hpp"

class CalculData
{
public:
  CalculData(const ass::InitData &initData);
  ~CalculData();

private:

  /***
   * @brief Функция расчета дневной нормы воды.
   * 
   * @param Sex Пол: true - мужской, false - женский
   * @param Weight Вес пользователя
   * 
   * @return Число (дневная норма воды)
   */
  int CalculWater();

  /***
   * @brief Функция расчета дневной нормы калорий.
   * Расчет проводиться за формулой Харриса-Бенедикта
   * Базовая формула для мужчин: 88,36+(13,4*вес, кг)+(4,8*рост, см)-(5,7*возраст, лет)
   * Базовая формула для женщин: 447,6+(9,2*вес, кг)+(3,1*рост, см)-(4,2*возраст, лет)
   * 
   * @param Sex Пол: true - мужской, false - женский
   * @param Weight Вес пользователя
   * @param Growth Рост пользователя
   * @param BirthDay Год  рожденния рождения
   * @param Activity Днневная активность: -1 - сидячая, 0 - нормальная, 1 - высокая
   * 
   * @return Число (дневная норма калорий)
   */
  int CalculCalories();

  /***
   * @brief Функция расчета дневной нормы жиров.
   * 
   * @param Calories Дневная норма калорий 
   * @return Число (дневная норма жиров в грамах)
   */
  int CalculFats();

  /***
   * @brief Функция расчета дневной нормы углеводов.
   * 
   * @param Calories Дневная норма калорий 
   * @return Число (дневная норма углеводов в грамах)
   */
  int CalculCarbon();

  /***
   * @brief Функция расчета дневной нормы белков.
   * 
   * @param Calories Дневная норма калорий 
   * @return Число (дневная норма белков в грамах)
   */
  int CalculProteins();

  /***
   * @brief Функция считует существующие даные из файла.
   *    Если файла нету - создаст файл и пустой объект.
   */
  void read_file_calcul_data();

  ass::InitData initData;
  ass::CalculData calculData;
  Json m_file_calcul_data_json;
};

#endif //GTKMM_CALCULDATA_HPP