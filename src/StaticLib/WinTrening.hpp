/* -*- С++ -*- */
#ifndef GTKMM_WINTRENING_HPP
#define GTKMM_WINTRENING_HPP

#include "FitnessAssistant.hpp"

using Json = nlohmann::json;

class WinTrening
{
public:
  WinTrening(bool isRoot);

  Gtk::ScrolledWindow &getScrolledWindow();

private:
  /***
   * Класс Exercises, создаёт заполеный обьек типа Gtk::HBox, который на выходе
   * будет содержать готовую структуру записи даных об упражнении: пит, изображение,
   * название, описание. Созданно для упрощения записи новых даных в файл и вывода
   * на окно.
   * 
   * @param path_image Путь к изображению.
   * @param title Название упражнения.
   * @param text Краткое описание упражнения
   * 
   * @return Gtk::HBox с заполнеными даными
   */
  struct Exercises : public Gtk::HBox
  {
  public:
    Exercises(std::string title, std::string path_image, std::string text);

  private:
    Gtk::VBox m_VBox_Main;
    Gtk::HBox m_HBox_Title,
        m_HBox_Body,
        m_HBox_Text;
    Gtk::Box m_Box_Image;
    Gtk::Label m_Label_Title,
        m_Label_Text;
    Gtk::Image image;
  };

  /* Содержит все данные о записях упражнений */
  struct Record
  {
    std::string type;                               // тип упаржнения (група мишц)
    std::string title;                              // заголовок
    std::string icon_path = "Image/add_record.png"; // путь к изображению
    std::string text;                               // краткое описание упражнения
  };

  /* Реализует окно добавления новых упражнений */
  struct AddRecord : Gtk::Window
  {
  public:
    AddRecord();

  private:
    /* Структура выпадающих подсказок */
    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:
      ModelColumns() { add(m_col_name); }

      Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    };

    ModelColumns m_Columns;

    /***
     * Добаляет новые записи в файл FILE_TRENING_DATA, также проверяет
     * коретность входных данных, проверяет существует ли уже такая 
     * запись в файле.
     */
    void on_button_add();

    /* закрытие окна добавления записей*/
    void on_button_cancel();

    /***
     * Считует из файла FILE_TRENING_DATA все существующие типы, 
     * для вывода в выпадающюю подсказку.
     * 
     * @param str Вектор для хранения ексклюзивных типов
     */
    void list_record_type(std::vector<std::string> &str);

    WinTrening::Record *record;
    std::vector<std::string> str_type; // хранит все ексклюзивные типы упражнений

    Gtk::VBox m_VBox_Main, m_VBox_Text, m_VBox_Title, m_VBox_Type;
    Gtk::HBox m_HBox_Title, m_HBox_Text, m_HBox_Type;
    Gtk::ButtonBox m_butonbox;
    Gtk::Label m_label_title, m_label_text, m_label_type;
    Gtk::Entry m_entry_title, m_entry_type;

    Gtk::Button m_button_add, m_button_cancel;
    Gtk::ScrolledWindow m_ScrolledWindow_Buffer;
    Glib::RefPtr<Gtk::TextBuffer> m_refTextBuffer;
    Gtk::TextView m_TextView;
    Gtk::ComboBoxText m_comboboxtext_type;
  };

  /***
   * Реализует окно удаления упражнений из файла FILE_TRENING_DATA.
   */
  struct DeleteRecord : Gtk::Window
  {
  public:
    DeleteRecord();

  private:
      /* Структура выпадающих подсказок */
    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:
      ModelColumns() { add(m_col_name); }

      Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    } m_Columns, m_Columns_title;

    /* Функция-обработчик, закрытие окна удаления упражнений */
    void on_button_cancel();

    /* Функция-обработчик, реализует удаление выбраной записи из файла */
    void on_button_delete();

    /***
     * Функция-обработчик, ищет в фале выбраную запись.
     * В случае успеха выведит на окно эту запись, иначе выведит сообщение
     * о неудачном поиске
     */
    void on_button_find();
       /***
     * Считует из файла FILE_TRENING_DATA все существующие типы, 
     * для вывода в выпадающюю подсказку.
     * 
     * @param str Вектор для хранения ексклюзивных типов
     */
    void list_record_type(std::vector<std::string> &str, std::vector<std::string> &str_title);

    std::vector<std::string> str_type; // хранит все ексклюзивные типы упражнений
    std::vector<std::string> str_title; // хранит все имена упражнений для выпадающей подсказки
    WinTrening::Record *record;
    WinTrening::Record static_record; // хранит выбраную запись

    Gtk::VBox m_VBox_Main, m_VBox_Title, m_VBox_Type;
    Gtk::HBox m_HBox_Type, m_HBox_Title, m_HBox_Show;
    Gtk::Label m_label_type, m_label_title;
    Gtk::Entry m_entry_type, m_entry_title;
    Gtk::ButtonBox m_butonbox;
    Gtk::Button m_button_delete, m_button_cancel, m_button_find;
  };

  /***
   * Структура вкладок страниц.
   * 
   * @param index Особеный индетификатор вкладки
   * @param type Название вкладки
   */
  struct Page
  {
    Page(int index, std::string type) : index(index), type(type){};

    int index;        // индетификатор вкладки
    std::string type; // название вкладки
  };

  /* Реализует окно редактирования упражнений */
  struct EditRecord : Gtk::Window
  {
  public:
    EditRecord();

  private:
    /* Структура выпадающей подсказки */
    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:
      ModelColumns() { add(m_col_name); }

      Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    };

    ModelColumns m_Columns_type, m_Columns_title;

    /***
     * Добаляет отредактированую записи в файл FILE_TRENING_DATA, также 
     * проверяет коретность входных данных, проверяет существует ли уже
     * такая запись в файле.
     */
    void on_button_save();

    /***
     * Функция-обработчик, ищет в фале выбраную запись.
     * В случае успеха выведит даные о этой записи, иначе выведит сообщение
     * о неудачном поиске
     */
    void on_button_find();

    /* Функция-обработчик, закрытие окна редактирования упражнений */
    void on_button_cancel();

    /***
     * Считует из файла FILE_TRENING_DATA все существующие типы и названия упражнений, 
     * для вывода в выпадающюю подсказку.
     * 
     * @param str_type Вектор для хранения ексклюзивных типов
     * @param str_title Вектор для хранения имён
     */
    void list_record_type(std::vector<std::string> &str_type, std::vector<std::string> &str_title);

    WinTrening::Record *record;
    WinTrening::Record static_record;   // хранит выбраную запись
    std::vector<std::string> str_type;  // хранит все типы упражнений для выпадающей подсказки
    std::vector<std::string> str_title; // хранит все имена упражнений для выпадающей подсказки

    Gtk::VBox m_VBox_Main, m_VBox_Text, m_VBox_Title, m_VBox_Type;
    Gtk::HBox m_HBox_Title, m_HBox_Text, m_HBox_Type;
    Gtk::ButtonBox m_butonbox;
    Gtk::Label m_label_title, m_label_text, m_label_type;
    Gtk::Entry m_entry_title, m_entry_type;

    Gtk::Button m_button_save, m_button_cancel, m_button_find;
    Gtk::ScrolledWindow m_ScrolledWindow_Buffer;
    Glib::RefPtr<Gtk::TextBuffer> m_refTextBuffer;
    Gtk::TextView m_TextView;
    Gtk::ComboBoxText m_comboboxtext_type;
  };

  std::vector<WinTrening::Page> page;       // информация о всех вкладках
  std::vector<Gtk::ScrolledWindow> scr_win; // окна всех вкладок
  std::vector<Gtk::VBox> vbox;              // хранит обьекты всех записей

  /***
   * Считует из файла FILE_TRENING_DATA все записи и выводит их в окно
   * сортируя по типам.
   */
  void read_record();

  /***
   * Считует из файла FILE_TRENIG_DATA только ексклюзивные типы упражнений
   * и создаёт в окне вкладки под каждый ексклюзивный тип.
   */
  void created_page();

  /* Функция-обработчик, вызывает обьект класа DeleteRecord для удаления записей */
  void on_button_delete();

  /* Функция-обработчик, вызывает обьект класа AddRecord для добавления записей */
  void on_button_add();

  /* Функция-обработчик, вызывает обьект класа EditRecord для редактирования записей */
  void on_button_edit();

  /* Функция-обработчик, закрывает окно тренировок*/
  void on_button_quit();

  int currentPage; // номер страницы для восстановлениея после удаления элемента

  WinTrening::Record *record;
  WinTrening::Exercises *exe;

  Gtk::Box m_VBox_Main, m_VBox_Chest_Main;
  Gtk::VBox m_VBox_Chest;
  Gtk::Notebook m_Notebook_Exercises;
  Gtk::ScrolledWindow m_ScrolledWindow;
  Gtk::ButtonBox m_ButtonBox;
  Gtk::Button m_Button_Edit, m_Button_Delete, m_Button_Add;
};

#endif //GTKMM_WINTRENING_HPP