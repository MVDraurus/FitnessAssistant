/* -*- c++ -*- */
#ifndef GTKMM_WININITDATA_HPP
#define GTKMM_WININITDATA_HPP

#include "FitnessAssistant.hpp"
#include "CalculData.hpp"
#include <ctype.h> 

/***
 * Класс реализует окно ввода начальных данных пользователя.
 * Класс создаёт новое собственоне окно с полями для ввода начальных.
 * Также класс реалезут запись всех данных в файл. Во время выполнения
 * обьекта класса, остаёться доступным окно, которое его вызвало.
 * 
 * @param n_usr Порядковый номер записи пользователя в файле FILE_USER_INIT_DATA
 * @param is_root <tt>true<\tt>-алминистратор, <tt>false<\tt>-пользователь
 * @param newusr <tt>true<\tt>-создать нового пользователя, 
 * <tt>false<\tt>-редактирование профиля
 */
class WinInitData : public Gtk::Window
{
  public:
    WinInitData(int n_usr, bool is_root, bool newusr);
    ~WinInitData();

  private:
    /* Завершение сеанса */
    void on_button_quit();

    /***
     * Функция записывает физические данные пользователя в файл;
     * Вызываеться при изменении настроек или при начальной 
     * инициализации пользователя;
     */
    void on_button_save();

    /***
     * Функция считывает информацию о физических данных пользователя
     * и записывает их в структуру UserInitData.
     * Нужна для того, чтобы при открытии настроек пользователем
     * ему передоставлялись его данные, в следствии чего, пользователю
     * нужно будет изменить только нужное ему поле.
     */
    void set_usr_init_data();

    /***
     * Функция-обработчик on_button_icon_find(), вызываеться при нажатии на
     * иконку поиска, считует имя из поля m_Entery_Account, сравнивает с всеми
     * зарезервироваными пользователями в системе и выводи все данные о
     * пользователе. 
     */
    void on_button_icon_find(Gtk::EntryIconPosition, const GdkEventButton *);

    /***
     * Функция-обработчик on_button_enter_find(), вызываеться при нажатии на
     * <Enter> в поле m_Entery_Account, считует имя из поля m_Entery_Account,
     * сравнивает с всеми зарезервироваными пользователями в системе и 
     * выводи все данные о пользователе. 
     */
    void on_button_enter_find();

    /***
     * Функция set_list_user(), считывает имена всех зарезервированых 
     * пользователей из файла FILE_USER_INIT_DATA и добавляет их в подсказку,
     * которая будет отображаться при поиске.
     */
    void set_list_user();

    /***
     * Функция-обработчик output_user_data(), ищет в файле FILE_USR_INIT_DATA
     * пользователя с таким названием как введено в строке поиска,
     * в случае успеха выведет все даные о пользователе, иначе выведет
     * сообщение что такого пользователя ненайденно.
     */
    void output_user_data();

    /***
     * Функция-обработчик если кнопка показать пароль активна - покажет пароль,
     * если нет пароль будет скрытый.
     */
    void on_checkbox_visibility();

    /* Реализует удаление выбраного пользователя из системы */
    void on_button_del_usr();
    
    /* Реализует вывод подсказки, в поле поиска пользователей */

    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
      public:
        ModelColumns(){add(m_col_name);}

        Gtk::TreeModelColumn<Glib::ustring> m_col_name;
    };

    ModelColumns m_Columns;
    typedef std::map<int, Glib::ustring> type_actions_map;
    type_actions_map m_CompletionActions;

    ass::InitData *PUserInitData = new ass::InitData();

    bool _new_user, isRoot;
    int nUserInit;

    /* Дочирние виджеты */
    Gtk::VBox m_VBox_Main;
    Gtk::ButtonBox m_ButtonBox;
    Gtk::Button m_button_quit, m_button_save, m_button_del_usr;
    Gtk::Entry m_Entry_Name, m_Entry_Account, m_Entry_Passwd;
    Gtk::ComboBoxText m_Combo_Sex, m_Combo_Target, m_Combo_Activity;
    Gtk::CheckButton m_CheckButton_Visible;
    Gtk::Box m_HBox_Account,
        m_HBox_Name_Age,
        m_VBox_Name, m_VBox_Age,
        m_HBox_Weight_And_Growth, m_VBox_Current_Weight, m_VBox_Desired_Weight, m_VBox_Growth,
        m_HBox_Sex_Target_Activity, m_VBox_Sex, m_VBox_Target, m_VBox_Activity,
        m_VBox_Passwd, m_HBox_Visible_Label;

    Gtk::Label m_Label_Account,
        m_Label_Name, m_Label_Age,
        m_Label_Current_Weight, m_Label_Desired_Weight, m_Label_Growth,
        m_Label_Sex, m_Label_Activity, m_Label_Target,
        m_Label_Passwd;

    Glib::RefPtr<Gtk::Adjustment> m_adjustment_age,
        m_Adjustment_Current_Weight, m_Adjustment_Desired_Weight,
        m_Adjustment_Growth;

    Gtk::SpinButton m_SpinButton_Age,
        m_SpinButton_Current_Weight, m_SpinButton_Desired_Weight, m_SpinButton_Growth;
};

#endif //GTKMM_WININITDATA_HPP