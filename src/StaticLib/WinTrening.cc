#include "WinTrening.hpp"

WinTrening::WinTrening(bool isRoot)
    : m_VBox_Main(Gtk::ORIENTATION_VERTICAL),
      m_Button_Edit(resources.edit),
      m_Button_Delete(resources.del),
      m_Button_Add(resources.add)
{
    m_VBox_Main.pack_start(m_Notebook_Exercises);

    WinTrening::created_page();
    WinTrening::read_record();

    /********** Нижний бокс **********/
    if (isRoot == true)
    {
        m_VBox_Main.pack_start(m_ButtonBox, Gtk::PACK_SHRINK);
        m_ButtonBox.pack_start(m_Button_Edit, Gtk::PACK_EXPAND_PADDING, 5);
        m_ButtonBox.pack_start(m_Button_Delete, Gtk::PACK_EXPAND_PADDING, 5);
        m_ButtonBox.pack_start(m_Button_Add, Gtk::PACK_EXPAND_PADDING, 5);

        m_ButtonBox.set_border_width(5);
        m_ButtonBox.set_layout(Gtk::BUTTONBOX_END);
        m_Button_Add.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::on_button_add));
        m_Button_Delete.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::on_button_delete));
        m_Button_Edit.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::on_button_edit));
    }

    m_ScrolledWindow.add(m_VBox_Main);
}

Gtk::ScrolledWindow &WinTrening::getScrolledWindow()
{
    return this->m_ScrolledWindow;
}

void WinTrening::on_button_add()
{
    WinTrening::AddRecord addrec;
    Gtk::Main::run(addrec);

    currentPage = m_Notebook_Exercises.get_current_page();

    /* Удаляю всё содержимое окон */
    page.clear();
    scr_win.clear();
    vbox.clear();

    /* Добавляю новые записи в окно */
    WinTrening::created_page();
    WinTrening::read_record();

    m_Notebook_Exercises.show_all_children();
    m_Notebook_Exercises.set_current_page(currentPage);
}

void WinTrening::on_button_delete()
{
    WinTrening::DeleteRecord delrec;
    Gtk::Main::run(delrec);

    currentPage = m_Notebook_Exercises.get_current_page();

    /* Удаляю всё содержимое окон */
    page.clear();
    scr_win.clear();
    vbox.clear();

    /* Добавляю новые записи в окно */
    WinTrening::created_page();
    WinTrening::read_record();

    m_Notebook_Exercises.show_all_children();
    m_Notebook_Exercises.set_current_page(currentPage);
}

void WinTrening::on_button_edit()
{
    WinTrening::EditRecord editrec;
    Gtk::Main::run(editrec);

    currentPage = m_Notebook_Exercises.get_current_page();

    /* Удаляю всё содержимое окон */
    page.clear();
    scr_win.clear();
    vbox.clear();

    /* Добавляю новые записи в окно */
    WinTrening::created_page();
    WinTrening::read_record();

    m_Notebook_Exercises.show_all_children();
    m_Notebook_Exercises.set_current_page(currentPage);
}

void WinTrening::read_record()
{
    Json json = Json::array();
    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    for (const auto &obj : json)
    {
        for (size_t i(0); i < page.size(); i++)
        {
            if (page.at(i).type.compare(obj["type"].get<std::string>()) == 0)
            {
                exe = new WinTrening::Exercises(obj["title"].get<std::string>(),
                                                obj["icon"].get<std::string>(),
                                                obj["description"].get<std::string>());
                vbox.at(i).pack_start(*exe, Gtk::PACK_SHRINK);
                break;
            }
        }
    }
}

void WinTrening::created_page()
{
    Json json = Json::array();
    bool type_exist(false);
    size_t count(0);

    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    for (auto &obj : json)
    {
        type_exist = false;

        for (size_t i(0); i < page.size(); i++)
            if (page.at(i).type.compare(obj["type"].get<std::string>()) == 0)
            {
                type_exist = true;
                break;
            }

        if (type_exist == false)
            page.push_back(WinTrening::Page(count++, obj["type"].get<std::string>()));
    }

    for (size_t i(0); i < page.size(); i++)
    {
        scr_win.resize(scr_win.size() + 1);
        scr_win.resize(scr_win.size() + 1);
        scr_win.back().set_policy(Gtk::POLICY_NEVER, Gtk::POLICY_AUTOMATIC);

        vbox.resize(vbox.size() + 1);
        scr_win.at(i).add(vbox.at(i));
        m_Notebook_Exercises.append_page(scr_win.at(i), page.at(i).type);
    }
}

WinTrening::Exercises::Exercises(std::string title, std::string path_image, std::string text)
    : m_Label_Title(title), image(path_image), m_Label_Text(text)
{
    this->pack_start(m_VBox_Main, Gtk::PACK_SHRINK);
    this->set_border_width(5);

    /********** Заголовок **********/
    m_Label_Title.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);
    m_HBox_Title.pack_start(m_Label_Title, Gtk::PACK_SHRINK);
    m_Label_Title.set_justify(Gtk::JUSTIFY_LEFT);
    m_Label_Title.override_font(Pango::FontDescription("Times New Roman 12"));

    m_Label_Title.set_size_request(-1, 40);
    m_HBox_Title.set_border_width(5);

    m_VBox_Main.pack_start(m_HBox_Title, Gtk::PACK_SHRINK);

    /********** Картинка **********/
    m_Box_Image.pack_start(image, Gtk::PACK_SHRINK);

    m_HBox_Body.pack_start(m_Box_Image, Gtk::PACK_SHRINK);

    /********** Текст **********/
    m_HBox_Text.set_border_width(10);
    m_Label_Text.set_line_wrap();
    m_Label_Text.override_font(Pango::FontDescription("Times New Roman 11"));

    m_HBox_Text.pack_start(m_Label_Text, Gtk::PACK_SHRINK);
    m_HBox_Body.pack_start(m_HBox_Text, Gtk::PACK_SHRINK);

    m_VBox_Main.pack_start(m_HBox_Body, Gtk::PACK_SHRINK);
}

WinTrening::AddRecord::AddRecord()
    : m_button_add(resources.add),
      m_button_cancel(resources.cancel),
      m_label_title(resources.name_exercise),
      m_label_text(resources.short_name_exercise),
      m_label_type(resources.type_exercise)
{
    /********** Параметры окна **********/
    this->set_title(resources.title_addrecord);
    this->set_border_width(10);
    this->set_position(Gtk::WIN_POS_CENTER);
    this->set_default_size(500, 350);
    this->set_resizable(false);

    /********** Создаю выпадающий список возможных типов **********/
    WinTrening::AddRecord::list_record_type(str_type); // запись в вектор список всех типов

    auto completion = Gtk::EntryCompletion::create();
    m_entry_type.set_completion(completion);

    auto refCompletionModel = Gtk::ListStore::create(m_Columns);
    completion->set_model(refCompletionModel);

    Gtk::TreeModel::Row row = *(refCompletionModel->append());

    for (size_t i(0); i < str_type.size(); i++)
    {
        row = *(refCompletionModel->append());
        row[m_Columns.m_col_name] = str_type.at(i);
    }

    completion->set_text_column(m_Columns.m_col_name);

    /********** Тип упражнения *********/
    m_VBox_Type.pack_start(m_label_type);

    m_entry_type.set_max_length(63);
    m_VBox_Type.pack_start(m_entry_type);
    m_HBox_Type.pack_start(m_VBox_Type);

    /********** Заголовок *********/
    m_VBox_Title.pack_start(m_label_title);

    m_entry_title.set_max_length(63);
    m_VBox_Title.pack_start(m_entry_title);
    m_HBox_Title.pack_start(m_VBox_Title);

    /********** Описание *********/
    m_refTextBuffer = Gtk::TextBuffer::create();
    m_TextView.set_buffer(m_refTextBuffer);
    // m_refTextBuffer.set_max_length(850);

    m_ScrolledWindow_Buffer.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    m_ScrolledWindow_Buffer.set_border_width(5);
    m_ScrolledWindow_Buffer.add(m_TextView);
    m_VBox_Text.pack_start(m_label_text, Gtk::PACK_SHRINK);
    m_VBox_Text.pack_start(m_ScrolledWindow_Buffer);
    m_HBox_Text.pack_start(m_VBox_Text);

    /********** контейнер кнопок *********/
    m_butonbox.pack_start(m_button_cancel);
    m_butonbox.pack_start(m_button_add);

    m_button_cancel.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::AddRecord::on_button_cancel));
    m_button_add.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::AddRecord::on_button_add));

    /********** главные контейнеры *********/
    m_VBox_Main.pack_start(m_HBox_Type, Gtk::PACK_SHRINK, 5);
    m_VBox_Main.pack_start(m_HBox_Title, Gtk::PACK_SHRINK, 5);
    m_VBox_Main.pack_start(m_HBox_Text);
    m_VBox_Main.pack_start(m_butonbox, Gtk::PACK_SHRINK, 5);

    this->add(m_VBox_Main);
    this->show_all_children();
}

void WinTrening::AddRecord::on_button_add()
{
    Json json = Json::array();
    std::string title(m_entry_title.get_text()),
        type(m_entry_type.get_text()),
        description(m_refTextBuffer->get_text());
    size_t index(0);

    // чтение всех объектов из файла
    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    // поиск существующего
    for (const auto &obj : json)
    {
        if ((title.compare(obj["title"].get<std::string>()) == 0) &&
            (type.compare(obj["type"].get<std::string>()) == 0))
        {
            m_label_title.set_text(resources.exercise_name_exist);
            m_label_title.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

            m_label_type.set_text(resources.type_exercise);
            m_label_type.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

            m_label_text.set_text(resources.short_name_exercise);
            m_label_text.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

            return;
        }
        else
        {
            m_label_title.set_text(resources.name_exercise);
            m_label_title.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);
        }

        if (type.empty())
        {
            m_label_type.set_text(resources.empty_line);
            m_label_type.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

            m_label_title.set_text(resources.name_exercise);
            m_label_title.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

            m_label_text.set_text(resources.short_name_exercise);
            m_label_text.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

            return;
        }
        else
        {
            m_label_type.set_text(resources.type_exercise);
            m_label_type.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);
        }

        if (title.empty())
        {
            m_label_title.set_text(resources.empty_line);
            m_label_title.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

            m_label_type.set_text(resources.type_exercise);
            m_label_type.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

            m_label_text.set_text(resources.short_name_exercise);
            m_label_text.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

            return;
        }
        else
        {
            m_label_type.set_text(resources.type_exercise);
            m_label_type.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);
        }

        if (description.empty())
        {
            m_label_text.set_text(resources.empty_line);
            m_label_text.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

            m_label_type.set_text(resources.type_exercise);
            m_label_type.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

            m_label_title.set_text(resources.name_exercise);
            m_label_title.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

            return;
        }
        else
        {
            m_label_text.set_text(resources.short_name_exercise);
            m_label_text.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);
        }
    }

    // создание новой записи
    Json new_exercise = Json::object();
    new_exercise["description"] = description;
    new_exercise["icon"] = "Image/add_record.png";
    new_exercise["title"] = title;
    new_exercise["type"] = type;

    // добавление записи
    json.push_back(new_exercise);

    // запись в файл
    std::ofstream ofs(FILE_TRENING_DATA_JSON, std::ios_base::binary | std::ios_base::out | std::ios_base::trunc);
    ofs << json.dump(SPACE_IN_JSON_FILES);
    ofs.close();

    WinTrening::AddRecord::on_button_cancel();
}

void WinTrening::AddRecord::on_button_cancel()
{
    this->hide();
}

void WinTrening::AddRecord::list_record_type(std::vector<std::string> &str)
{
    Json json = Json::array();

    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    for (const auto &obj : json)
    {
        std::string type(obj["type"].get<std::string>());

        if (std::find(str.begin(), str.end(), type) == str.end())
            str.push_back(type);
    }
}

WinTrening::DeleteRecord::DeleteRecord()
    : m_button_cancel(resources.cancel),
      m_button_delete(resources.del),
      m_button_find(resources.find),
      m_label_type(resources.enter_type_exercise),
      m_label_title(resources.enter_name_exercise)
{
    /********** Параметры окна **********/
    this->set_title(resources.title_deleterecord);
    this->set_border_width(5);
    this->set_position(Gtk::WIN_POS_CENTER);
    this->set_default_size(300, 100);

    /********** Создаю выпадающий список возможных типов **********/
    WinTrening::DeleteRecord::list_record_type(str_type, str_title); // запись в вектор список всех типов

    auto completion = Gtk::EntryCompletion::create();
    m_entry_type.set_completion(completion);

    auto refCompletionModel = Gtk::ListStore::create(m_Columns);
    completion->set_model(refCompletionModel);

    Gtk::TreeModel::Row row = *(refCompletionModel->append());

    for (size_t i(0); i < str_type.size(); i++)
    {
        row = *(refCompletionModel->append());
        row[m_Columns.m_col_name] = str_type.at(i);
    }

    completion->set_text_column(m_Columns.m_col_name);

    /********** Ввод типа **********/
    m_VBox_Type.pack_start(m_label_type);
    m_VBox_Type.pack_start(m_entry_type);

    m_HBox_Type.pack_start(m_VBox_Type);
    m_VBox_Main.pack_start(m_HBox_Type, Gtk::PACK_SHRINK, 0);

    /********** Создаю выпадающий список возможных названий **********/
    auto completion_title = Gtk::EntryCompletion::create();
    m_entry_title.set_completion(completion_title);

    auto refCompletionModel_title = Gtk::ListStore::create(m_Columns_title);
    completion_title->set_model(refCompletionModel_title);

    Gtk::TreeModel::Row row_title = *(refCompletionModel_title->append());

    for (size_t i(0); i < str_title.size(); i++)
    {
        row_title = *(refCompletionModel_title->append());
        row_title[m_Columns_title.m_col_name] = str_title.at(i);
    }

    completion_title->set_text_column(m_Columns_title.m_col_name);

    /********** Ввод названия **********/
    m_VBox_Title.pack_start(m_label_title);
    m_VBox_Title.pack_start(m_entry_title);
    // m_entry_title.set_icon_from_icon_name("edit-find", Gtk::ENTRY_ICON_SECONDARY);

    m_HBox_Title.pack_start(m_VBox_Title);
    m_VBox_Main.pack_start(m_HBox_Title, Gtk::PACK_SHRINK, 0);

    /********** Ввод записи **********/

    m_VBox_Main.pack_start(m_HBox_Show, Gtk::PACK_SHRINK, 0);

    /********** Контейнер кнопок **********/
    // m_butonbox.set_border_width(5);
    // m_butonbox.set_layout(Gtk::BUTTONBOX_CENTER);

    m_butonbox.pack_start(m_button_cancel, Gtk::PACK_SHRINK);
    m_butonbox.pack_start(m_button_find, Gtk::PACK_SHRINK);
    m_butonbox.pack_start(m_button_delete, Gtk::PACK_SHRINK);
    m_butonbox.set_spacing(6);

    m_button_delete.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::DeleteRecord::on_button_delete));
    m_button_cancel.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::DeleteRecord::on_button_cancel));
    m_button_find.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::DeleteRecord::on_button_find));

    m_VBox_Main.pack_start(m_butonbox, Gtk::PACK_SHRINK);
    m_VBox_Main.set_spacing(6);

    /********** Виджеты окна **********/
    m_VBox_Main.set_border_width(5);
    this->add(m_VBox_Main);
    this->show_all_children();
}

void WinTrening::DeleteRecord::list_record_type(std::vector<std::string> &str, std::vector<std::string> &str_title)
{
    Json json = Json::array();

    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    for (const auto &obj : json)
    {
        std::string title(obj["title"].get<std::string>()),
            type(obj["type"].get<std::string>());

        if (std::find(str_type.begin(), str_type.end(), type) == str_type.end())
            str_type.push_back(type);

        if (std::find(str_title.begin(), str_title.end(), title) == str_title.end())
            str_title.push_back(title);
    }
}

void WinTrening::DeleteRecord::on_button_cancel()
{
    this->hide();
}

void WinTrening::DeleteRecord::on_button_delete()
{
    Json json = Json::array();
    bool record_exist(false);

    // чтение всех объектов из файла
    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    if (static_record.type.empty() || static_record.title.empty())
    {
        static_record.title = m_entry_title.get_text();
        static_record.type = m_entry_type.get_text();
    }

    // поиск и удаление объекта
    for (size_t idx(0); idx < json.size(); idx++)
    {
        if ((static_record.type.compare(json[idx]["type"].get<std::string>()) == 0) &&
            (static_record.title.compare(json[idx]["title"].get<std::string>()) == 0))
        {
            record_exist = true;

            /********** Окно подтведжения **********/
            Gtk::MessageDialog msg(*this, resources.delete_record, false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO);
            msg.set_secondary_text(resources.warning_delete_record);

            switch (msg.run())
            {
            case Gtk::RESPONSE_YES:
            {
                // удаление
                json.erase(idx);

                // запись обратно в файл
                std::ofstream ofs(FILE_TRENING_DATA_JSON, std::ios_base::binary | std::ios_base::out | std::ios_base::trunc);
                ofs << json.dump(SPACE_IN_JSON_FILES);
                ofs.close();
                break;
            }
            default:
                break;
            }
        }
    }

    if (record_exist == false)
    {
        m_label_title.set_text(resources.msg_record_not_found);
        m_label_title.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

        static_record.type = "";
        static_record.title = "";

        // m_HBox_Show.remove(*static_exe);
        this->set_default_size(0, -1);
    }
    else
    {
        m_label_title.set_text(resources.enter_name_exercise);
        m_label_title.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);
        WinTrening::DeleteRecord::on_button_cancel();
    }
}

void WinTrening::DeleteRecord::on_button_find()
{
    Json json = Json::array();
    static WinTrening::Exercises *static_exe(new WinTrening::Exercises(
        static_record.title, static_record.icon_path, static_record.text));
    bool record_exist(true);
    std::string title(m_entry_title.get_text()),
        type(m_entry_type.get_text());

    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    // поиск записи
    for (const auto &obj : json)
    {
        std::string obj_title(obj["title"].get<std::string>()),
            obj_type(obj["type"].get<std::string>());

        if ((type.compare(obj_type) == 0) &&
            (title.compare(obj_title) == 0))
        {
            if ((type.compare(static_record.type) == 0) &&
                (title.compare(static_record.title) == 0))
                return;

            static_record.type = obj_type;
            static_record.title = obj_title;

            m_HBox_Show.remove(*static_exe);

            static_exe = new WinTrening::Exercises(obj_title,
                                                   obj["icon"].get<std::string>(),
                                                   obj["description"].get<std::string>());
            m_HBox_Show.pack_start(*static_exe, Gtk::PACK_SHRINK, 0);
            this->show_all_children();

            record_exist = false;
        }
    }

    if (record_exist == true)
    {
        m_label_title.set_text(resources.msg_record_not_found);
        m_label_title.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

        static_record.type = "";
        static_record.title = "";

        m_HBox_Show.remove(*static_exe);
        this->set_default_size(0, -1);
    }
    else
    {
        m_label_title.set_text(resources.enter_name_exercise);
        m_label_title.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);
    }
}

WinTrening::EditRecord::EditRecord()
    : m_button_save(resources.save),
      m_button_find(resources.find_clear),
      m_button_cancel(resources.cancel),
      m_label_title(resources.name_exercise),
      m_label_text(resources.short_name_exercise),
      m_label_type(resources.name_exercise)
{
    /********** Параметры окна **********/
    this->set_title(resources.edit_exercise);
    this->set_border_width(10);
    this->set_position(Gtk::WIN_POS_CENTER);
    this->set_default_size(500, 350);
    this->set_resizable(false);

    WinTrening::EditRecord::list_record_type(str_type, str_title); // запись в вектор список всех типов

    /********** Создаю выпадающий список возможных типов **********/
    auto completion_type = Gtk::EntryCompletion::create();
    m_entry_type.set_completion(completion_type);

    auto refCompletionModel_type = Gtk::ListStore::create(m_Columns_type);
    completion_type->set_model(refCompletionModel_type);

    Gtk::TreeModel::Row row_type = *(refCompletionModel_type->append());

    for (size_t i(0); i < str_type.size(); i++)
    {
        row_type = *(refCompletionModel_type->append());
        row_type[m_Columns_type.m_col_name] = str_type.at(i);
    }
    completion_type->set_text_column(m_Columns_type.m_col_name);

    /********** Создаю выпадающий список возможных названий **********/
    auto completion_title = Gtk::EntryCompletion::create();
    m_entry_title.set_completion(completion_title);

    auto refCompletionModel_title = Gtk::ListStore::create(m_Columns_title);
    completion_title->set_model(refCompletionModel_title);

    Gtk::TreeModel::Row row_title = *(refCompletionModel_title->append());

    for (size_t i(0); i < str_title.size(); i++)
    {
        row_title = *(refCompletionModel_title->append());
        row_title[m_Columns_title.m_col_name] = str_title.at(i);
    }
    completion_title->set_text_column(m_Columns_title.m_col_name);

    /********** Тип упражнения *********/
    m_VBox_Type.pack_start(m_label_type);

    m_entry_type.set_max_length(63);
    m_VBox_Type.pack_start(m_entry_type);
    m_HBox_Type.pack_start(m_VBox_Type);

    /********** Заголовок *********/
    m_VBox_Title.pack_start(m_label_title);

    m_entry_title.set_max_length(63);
    m_VBox_Title.pack_start(m_entry_title);
    m_HBox_Title.pack_start(m_VBox_Title);

    /********** Описание *********/
    m_refTextBuffer = Gtk::TextBuffer::create();
    m_TextView.set_buffer(m_refTextBuffer);
    // m_refTextBuffer.set_max_length(850);

    m_ScrolledWindow_Buffer.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    m_ScrolledWindow_Buffer.set_border_width(5);
    m_ScrolledWindow_Buffer.add(m_TextView);
    m_VBox_Text.pack_start(m_label_text, Gtk::PACK_SHRINK);
    m_VBox_Text.pack_start(m_ScrolledWindow_Buffer);
    m_HBox_Text.pack_start(m_VBox_Text);

    /********** контейнер кнопок *********/
    m_butonbox.pack_start(m_button_cancel);
    m_butonbox.pack_start(m_button_find);
    m_button_save.set_sensitive(false);
    m_butonbox.pack_start(m_button_save);

    m_button_cancel.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::EditRecord::on_button_cancel));
    m_button_save.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::EditRecord::on_button_save));
    m_button_find.signal_clicked().connect(sigc::mem_fun(*this, &WinTrening::EditRecord::on_button_find));

    /********** главные контейнеры *********/
    m_VBox_Main.pack_start(m_HBox_Type, Gtk::PACK_SHRINK, 5);
    m_VBox_Main.pack_start(m_HBox_Title, Gtk::PACK_SHRINK, 5);
    m_VBox_Main.pack_start(m_HBox_Text);
    m_VBox_Main.pack_start(m_butonbox, Gtk::PACK_SHRINK, 5);

    this->add(m_VBox_Main);
    this->show_all_children();
}

void WinTrening::EditRecord::on_button_find()
{
    Json json = Json::array();
    bool record_exist(true);
    std::string title(m_entry_title.get_text()),
        type(m_entry_type.get_text());

    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    // поиск записи
    for (const auto &obj : json)
    {
        std::string obj_title(obj["title"].get<std::string>()),
            obj_type(obj["type"].get<std::string>());

        if ((type.compare(obj_type) == 0) &&
            (title.compare(obj_title) == 0))
        {
            // if ((type.compare(static_record.type) == 0) &&
            //     (title.compare(static_record.title) == 0))
            //     return;

            static_record.type = obj_type;
            static_record.title = obj_title;

            m_refTextBuffer->set_text(obj["description"].get<std::string>());

            record_exist = false;
            break;
        }
    }

    if (record_exist == true)
    {
        m_label_title.set_text(resources.msg_record_not_found);
        m_label_title.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);
        m_button_save.set_sensitive(false);

        static_record.type = "";
        static_record.title = "";
        m_refTextBuffer->set_text("");
    }
    else
    {
        m_label_title.set_text(resources.enter_name_exercise);
        m_label_title.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

        m_button_save.set_sensitive(true);
    }
}

void WinTrening::EditRecord::on_button_cancel()
{
    this->hide();
}

void WinTrening::EditRecord::on_button_save()
{
    Json json = Json::array();
    std::string title(m_entry_title.get_text()),
        type(m_entry_type.get_text()),
        description(m_refTextBuffer->get_text());
    bool record_exist(false);

    if (type.empty())
    {
        m_label_type.set_text(resources.empty_line);
        m_label_type.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

        m_label_title.set_text(resources.name_exercise);
        m_label_title.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

        return;
    }
    else if (title.empty())
    {
        m_label_title.set_text(resources.empty_line);
        m_label_title.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

        m_label_type.set_text(resources.name_exercise);
        m_label_type.override_color(Gdk::RGBA("white"), Gtk::STATE_FLAG_NORMAL);

        return;
    }

    // чтение всех объектов из файла
    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    // поиск дубликата
    for (auto &obj : json)
    {
        // исли после поиска было введено новое имя или тип, проверка, чтобы небыло совпадений
        if (((type.compare(obj["type"].get<std::string>()) == 0) && (title.compare(obj["title"].get<std::string>()) == 0)) &&
            ((type.compare(static_record.type) != 0) || (title.compare(static_record.title) != 0)))
        {
            record_exist = true;
            break;
        }
    }

    // если новое имя записи уже существует
    if (record_exist == true)
    {
        m_label_title.set_text(resources.exercise_exist);
        m_label_title.override_color(Gdk::RGBA("yellow"), Gtk::STATE_FLAG_NORMAL);

        return;
    }

    // обновление записи и сохранение в файл
    for (auto &obj : json)
    {
        // поиск записи
        if ((static_record.type.compare(obj["type"].get<std::string>()) == 0) &&
            (static_record.title.compare(obj["title"].get<std::string>()) == 0))
        {
            static_record.text = description;

            // замена значения
            obj["description"] = description;
            obj["title"] = title;
            obj["type"] = type;

            // запись обратно в файл
            std::ofstream ofs(FILE_TRENING_DATA_JSON, std::ios_base::binary | std::ios_base::out | std::ios_base::trunc);
            ofs << json.dump(SPACE_IN_JSON_FILES);
            ofs.close();

            break;
        }
    }

    WinTrening::EditRecord::on_button_cancel();
}

void WinTrening::EditRecord::list_record_type(std::vector<std::string> &str_type, std::vector<std::string> &str_title)
{
    Json json = Json::array();

    std::ifstream ifs(FILE_TRENING_DATA_JSON, std::ifstream::binary);
    ifs >> json;
    ifs.close();

    for (const auto &obj : json)
    {
        std::string title(obj["title"].get<std::string>()),
            type(obj["type"].get<std::string>());

        if (std::find(str_type.begin(), str_type.end(), type) == str_type.end())
            str_type.push_back(type);

        if (std::find(str_title.begin(), str_title.end(), title) == str_title.end())
            str_title.push_back(title);
    }
}
