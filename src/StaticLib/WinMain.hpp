// -*- С++ -*-
#ifndef GTKMM_WINMAIN_HPP
#define GTKMM_WINMAIN_HPP

#include "FitnessAssistant.hpp"
#include "WinInitData.hpp"
#include "WinTrening.hpp"
#include "WinNutrition.hpp"
#include "WinStatistic.hpp"

/***
 * Класс WinMain, реализует главное окно програмы через которое производиться
 * вызов всех остальных модулей.
 * 
 * @param n_usr Порядковый номер записи структуры пользователя в файле 
 * FILE_USR_INIT_DATA
 * @param is_root <tt>true<\tt>-алминистратор, <tt>false<\tt>-пользователь
 */
class WinMain : public Gtk::Window
{
public:
  WinMain(int n_usr, bool is_root);

private:

  /* Завершение работы главного окна */
  void on_button_quit();

  /***
   * Структура WinSetting создаёт собстевое окно с настройками программы.
   * WinSetting вызывает функции обьекта класса WinMain для изменения настроек
   * приложения.
   *
   * @param n_usr Порядковый номер записи структуры пользователя в файле 
   * FILE_USR_INIT_DATA
   * @param is_root <tt>true<\tt>-алминистратор, <tt>false<\tt>-пользователь
   */
  struct WinSetting
  {
  public:
    WinSetting(int n_usr, bool is_root);

    Gtk::ScrolledWindow &getScrolledWindow();

  private:
    /* Шаблон таблицы пользователей */
    class ModelColumns : public Gtk::TreeModel::ColumnRecord
    {
    public:
      ModelColumns()
      {
        add(m_col_num);
        add(m_col_name);
        add(m_col_age);
        add(m_col_current_weight);
        add(m_col_desired_weight);
        add(m_col_growth);
        add(m_col_sex);
        add(m_col_target);
        add(m_col_activity);
      }

      Gtk::TreeModelColumn<size_t> m_col_num;
      Gtk::TreeModelColumn<std::string> m_col_name;
      Gtk::TreeModelColumn<int> m_col_age;
      Gtk::TreeModelColumn<int> m_col_current_weight;
      Gtk::TreeModelColumn<int> m_col_desired_weight;
      Gtk::TreeModelColumn<int> m_col_growth;
      Gtk::TreeModelColumn<std::string> m_col_sex;
      Gtk::TreeModelColumn<std::string> m_col_target;
      Gtk::TreeModelColumn<std::string> m_col_activity;
    };

    ModelColumns *m_Columns = new ModelColumns;

    class ChangeLanguage : public Gtk::HBox
    {
    public:
      ChangeLanguage();

    private:
      void on_cell_data_extra(const Gtk::TreeModel::const_iterator &iter);

      //Signal handlers:
      void on_combo_changed();
      //Tree model columns:
      class ModelColumns : public Gtk::TreeModel::ColumnRecord
      {
      public:
        ModelColumns()
        {
          add(m_col_id);
          add(m_col_name);
        }

        Gtk::TreeModelColumn<int> m_col_id;
        Gtk::TreeModelColumn<Glib::ustring> m_col_name;
      };

      Json json = Json::object();
      ModelColumns m_Columns;
      Gtk::TreeModel::Row row;
      Gtk::ComboBox m_Combo;
      Gtk::CellRendererText m_cell;
      Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
    };

    /***
     * Функция on_button_changed_init_data() создаёт обьект класса WinInitData см.WinInitData.h
     * с параметром о изменении начальных данных и запустит окно поверх преддидущего, причём
     * преддидущее окно будет доступным.
     */
    void on_button_changed_init_data();

    /***
     * Функция on_button_new_user() создаёт обьект класса WinInitData см.WinInitData.h
     * с параметром о создании нового пользователя и запустит окно поверх преддидущего,
     * причём преддидущее окно будет доступным.
     */
    void on_button_new_user();

    /***
     * Функция fill_table(), считывает записи из файла FILE_USER_INIT_DATA и
     * заносит эти данные в таблицу.
     */
    void fill_table();

    /***
     * Функция on_commbo_sorting(), вызываеться при выборе типа сортировки
     * из кнопки m_comboboxtext_sorting, после чего она сканирует выбраный
     * вид сортировки и сортирует файл по выбраному признаку.
     */
    void on_combo_sorting();

    /***
     * Функция on_combo_filter(), вызываеться при выборе типа фильтрации
     * из кнопки m_comboboxtext_filter, после чего она сканирует выбраный
     * тип фильтрации и выводит в таблицу подходящий пользователей.
     */
    void on_combo_filter();

    /***
     * Функция on_combo_inquiry(), вызываеться при выборе типа запроса
     * из кнопки m_comboboxtext_inquiry, после чего она сканирует выбраный
     * запрос и выводит в отдельном окне результат запроса.
     */
    void on_combo_inquiry();

    bool isRoot;
    int nUserInit;

    Gtk::TreeView m_TreeView;
    Gtk::Frame m_frame_change_language;
    Glib::RefPtr<Gtk::ListStore> m_refTreeModel;
    Glib::RefPtr<Gtk::TreeSelection> m_refTreeSelection;
    Gtk::Label m_Label_Sorting, m_Label_filter, m_Label_inquiry;
    Gtk::VBox m_VBox_User_Main, m_VBox_User, m_VBox_Sorting, m_VBox_filter, m_VBox_inquiry;
    Gtk::TreeModel::Row row;
    Gtk::Notebook m_Notebook;
    Gtk::ScrolledWindow m_ScrolledWindow_User, m_ScrolledWindow_Setting, m_ScrolledWindow;
    Gtk::VBox m_VBox_Setting;
    Gtk::Button m_button_change_init_data, m_button_new_user;
    Gtk::ComboBoxText m_comboboxtext_sorting, m_comboboxtext_filter, m_comboboxtext_inquiry;
    Gtk::ButtonBox m_butonbox_bottom;
  };

  bool isRoot;
  int nUserInit;
  WinNutrition winNutrition;
WinStatistic winStatistic;
  Gtk::Fixed fixed;
  Gtk::Notebook m_Notebook;
  Gtk::ScrolledWindow m_ScrolledWindow_Trening;
  Gtk::ButtonBox top_button_box, bottom_button_box;
  Gtk::Button m_button_trening, m_button_food, m_button_statistic,
      m_button_quit, m_button_help, m_button_setting;
};

#endif // GTKMM_WINMAIN_HPP