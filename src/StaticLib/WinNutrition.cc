#include "WinNutrition.hpp"

WinNutrition::WinNutrition(int nUserInit, bool isRoot)
    : nUserInit(nUserInit), isRoot(isRoot), nUserStat(-1),
      m_frame_celcul_cal(resources.calcul_calories),
      m_frame_water(resources.uchet_water),
      m_frame_diaries(resources.diaries_nutrition),
      m_label_find(resources.find_user),
      m_label_BMR(resources.BMR),
      m_label_nutrition(resources.nutrition),
      m_label_target(resources.target),
      m_label_need(resources.need),
      m_button_add(resources.add),
      m_button_add_1(resources.plus),
      m_button_add_0_5(resources.plus),
      m_button_add_0_2(resources.plus),
      m_button_minus_1(resources.minus),
      m_button_minus_0_5(resources.minus),
      m_button_minus_0_2(resources.minus),
      m_frame_name(resources.name_dish),
      m_frame_protein(resources.protein_in_100g),
      m_frame_fat(resources.fat_in_100g),
      m_frame_carbon(resources.carbon_in_100g),
      m_frame_calories(resources.calories_in_100g),
      m_button_remove(resources.del),
      m_button_edit(resources.edit),
      m_button_add_diaries(resources.add),
      m_button_cancel_diaries(resources.back),
      new_user_json(Json::object()),
      new_date_json(Json::object()),
      m_file_statistic_json(Json::object()),
      user_json(Json::object()),
      date_json(Json::object()),
      diaries_records_json(Json::object()),
      today_date(get_date())
{
    new_date_json["Calories"] = 0.0f;
    new_date_json["Carbon"] = 0.0f;
    new_date_json["Fats"] = 0.0f;
    new_date_json["Proteins"] = 0.0f;
    new_date_json["Water"] = 0.0f;
    new_date_json["record"] = Json::array();

    /********** Поиск пользователя **********/
    if (isRoot == true)
    {
        m_VBox_Main.pack_start(m_VBox_Find, Gtk::PACK_SHRINK);

        m_entry_find.set_max_length(55);

        m_entry_find.set_icon_from_icon_name("edit-find", Gtk::ENTRY_ICON_SECONDARY);

        m_entry_find.signal_activate().connect(sigc::mem_fun(*this, &WinNutrition::on_button_enter_find));
        m_entry_find.signal_icon_press().connect(sigc::mem_fun(*this, &WinNutrition::on_button_icon_find));

        set_list_user(); // создаю список пользователей

        m_VBox_Find.pack_start(m_label_find);
        m_VBox_Find.pack_start(m_entry_find, Gtk::PACK_SHRINK);
    }
    else
    {
        fill_struct_initData();
    }

    /********** Расчёт калорий **********/
    /********** Необходимо **********/
    m_label_BMR.set_halign(Gtk::ALIGN_START);
    m_HBox_BMR.pack_start(m_label_BMR);

    m_label_BMR_data.set_halign(Gtk::ALIGN_END);
    m_HBox_BMR.pack_start(m_label_BMR_data);

    m_VBox_Calcul_Cal.pack_start(m_HBox_BMR, Gtk::PACK_EXPAND_WIDGET);

    /********** Питание **********/
    m_label_nutrition.set_halign(Gtk::ALIGN_START);
    m_HBox_Nutrition.pack_start(m_label_nutrition);

    m_label_nutrition_data.set_halign(Gtk::ALIGN_END);
    m_HBox_Nutrition.pack_start(m_label_nutrition_data);

    m_VBox_Calcul_Cal.pack_start(m_HBox_Nutrition);

    /********** Цель **********/
    m_label_target.set_halign(Gtk::ALIGN_START);
    m_HBox_Target.pack_start(m_label_target);

    m_label_target_data.set_halign(Gtk::ALIGN_END);
    m_HBox_Target.pack_start(m_label_target_data);

    m_VBox_Calcul_Cal.pack_start(m_HBox_Target);

    /********** Общие результаты **********/
    m_label_need.set_halign(Gtk::ALIGN_CENTER);
    m_HBox_Finish.pack_start(m_label_need);

    m_VBox_Calcul_Cal.pack_start(m_HBox_Finish);

    m_HBox_Calcul_Cal.pack_start(m_VBox_Calcul_Cal, Gtk::PACK_EXPAND_WIDGET);
    m_HBox_Calcul_Cal.set_border_width(BORDER_WIGHT_BOX);
    m_frame_celcul_cal.add(m_HBox_Calcul_Cal);

    m_VBox_Main.pack_start(m_frame_celcul_cal, Gtk::PACK_SHRINK);

    if (isRoot == false)
        WinNutrition::fill_struct_calculData(); // заполняю все поля

    /********** Учет воды **********/
    /********** 1 литр **********/
    m_button_add_1.override_font(Pango::FontDescription("Times New Roman 18"));
    m_VBox_Bottle_1.pack_start(m_button_add_1);
    bottle_1.set("Image/1L.png");
    m_button_minus_1.override_font(Pango::FontDescription("Times New Roman 18"));
    m_VBox_Bottle_1.pack_start(bottle_1);
    m_VBox_Bottle_1.pack_start(m_button_minus_1);
    m_VBox_Bottle_1.set_border_width(2);

    if (isRoot == false)
        m_HBox_Water.pack_start(m_VBox_Bottle_1);

    m_button_add_1.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_add_1));
    m_button_minus_1.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_minus_1));

    /********** 0,5 литр **********/
    m_button_add_0_5.override_font(Pango::FontDescription("Times New Roman 18"));
    m_VBox_Bottle_0_5.pack_start(m_button_add_0_5);
    bottle_0_5.set("Image/05L.png");
    m_button_minus_0_5.override_font(Pango::FontDescription("Times New Roman 18"));
    m_VBox_Bottle_0_5.pack_start(bottle_0_5);
    m_VBox_Bottle_0_5.pack_start(m_button_minus_0_5);
    m_VBox_Bottle_0_5.set_border_width(2);

    if (isRoot == false)
        m_HBox_Water.pack_start(m_VBox_Bottle_0_5);

    m_button_add_0_5.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_add_0_5));
    m_button_minus_0_5.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_minus_0_5));

    /********** 0,2 литр **********/
    m_button_add_0_2.override_font(Pango::FontDescription("Times New Roman 18"));
    m_VBox_Glass_0_2.pack_start(m_button_add_0_2);
    glass_0_2.set("Image/02L.png");
    m_button_minus_0_2.override_font(Pango::FontDescription("Times New Roman 18"));
    m_VBox_Glass_0_2.pack_start(glass_0_2);
    m_VBox_Glass_0_2.pack_start(m_button_minus_0_2);
    m_VBox_Glass_0_2.set_border_width(2);

    if (isRoot == false)
        m_HBox_Water.pack_start(m_VBox_Glass_0_2);

    m_button_add_0_2.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_add_0_2));
    m_button_minus_0_2.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_minus_0_2));

    /********** результат **********/
    if (isRoot == false)
    {
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << date_json.at("Water").get<float>();
        need_water.str("");
        need_water << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (calculData.Water / 1000.0);
        m_label_result_water.set_text(ss.str() + "/" + need_water.str() + " л.");
    }

    m_label_result_water.override_font(Pango::FontDescription("Times New Roman 18"));
    m_label_result_water.set_valign(Gtk::ALIGN_CENTER);
    m_label_result_water.set_halign(Gtk::ALIGN_CENTER);

    m_VBox_Result_Water.pack_start(m_label_result_water);
    m_VBox_Result_Water.set_border_width(BORDER_WIGHT_BOX);
    m_HBox_Water.pack_start(m_VBox_Result_Water);

    m_frame_water.add(m_HBox_Water);
    m_VBox_Main.pack_start(m_frame_water, Gtk::PACK_SHRINK);

    m_VBox_Diaries.set_border_width(BORDER_WIGHT_BOX);
    m_frame_diaries.add(m_VBox_Diaries);
    m_VBox_Main.pack_start(m_frame_diaries);

    m_refTreeModel = Gtk::ListStore::create(m_Columns_Diaries);
    m_TreeView.set_model(m_refTreeModel);

    if (isRoot == false)
    {
        for (auto &var : diaries_records_json)
        {
            ss.str("");
            row = *(m_refTreeModel->append());
            row[m_Columns_Diaries.m_col_id] = count++;
            row[m_Columns_Diaries.m_col_name] = var["Name"].get<std::string>().c_str();
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Calories").get<float>();
            row[m_Columns_Diaries.m_calories] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Carbon").get<float>();
            row[m_Columns_Diaries.m_carbon] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Fats").get<float>();
            row[m_Columns_Diaries.m_fats] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Proteins").get<float>();
            row[m_Columns_Diaries.m_proteins] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Weigth").get<float>();
            row[m_Columns_Diaries.m_weigth] = ss.str();
        }
    }

    //Add the TreeView's view columns:
    // m_TreeView.append_column("ID", m_Columns_Diaries.m_col_id);
    m_TreeView.append_column(resources.name, m_Columns_Diaries.m_col_name);
    m_TreeView.append_column(resources.calories, m_Columns_Diaries.m_calories);
    m_TreeView.append_column(resources.carbons, m_Columns_Diaries.m_carbon);
    m_TreeView.append_column(resources.fats, m_Columns_Diaries.m_fats);
    m_TreeView.append_column(resources.proteins, m_Columns_Diaries.m_proteins);
    m_TreeView.append_column(resources.weigth, m_Columns_Diaries.m_weigth);

    m_VBox_Diaries.pack_start(m_TreeView);

    m_butonbox.pack_start(m_button_add, Gtk::PACK_SHRINK);
    m_button_edit.set_sensitive(false);
    m_butonbox.pack_start(m_button_edit, Gtk::PACK_SHRINK);
    m_button_remove.set_sensitive(false);
    m_butonbox.pack_start(m_button_remove, Gtk::PACK_SHRINK);

    if (isRoot == false)
    {
        m_VBox_Diaries.pack_start(m_butonbox, Gtk::PACK_SHRINK);
        m_butonbox.set_layout(Gtk::BUTTONBOX_END);
        m_butonbox.set_spacing(SPACING_BUTTON_BOX);
    }

    m_button_add.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::show_window_add_diaries));
    m_button_remove.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_remove));
    m_button_edit.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::show_window_edit_diaries));

    m_refTreeSelection = m_TreeView.get_selection();
    m_refTreeSelection->signal_changed().connect(sigc::mem_fun(*this, &WinNutrition::on_selection_changed));

    m_ScrolledWindow_Main.add(m_VBox_Main);
    m_VBox_Main.set_border_width(BORDER_WIGHT_BOX);
}

void WinNutrition::on_selection_changed()
{
    auto is_selected = m_TreeView.get_selection()->get_selected();

    m_button_remove.set_sensitive(is_selected);
    m_button_edit.set_sensitive(is_selected);
}

void WinNutrition::on_button_remove()
{
    auto refSelection = m_TreeView.get_selection();
    if (refSelection)
    {
        auto iter = refSelection->get_selected();
        if (iter)
        {
            size_t id = (*iter)[m_Columns_Diaries.m_col_id];
            m_refTreeModel->erase(iter);

            date_json["Calories"] = date_json.at("Calories").get<float>() - diaries_records_json.at(id)["Calories"].get<float>();
            date_json["Carbon"] = date_json.at("Carbon").get<float>() - diaries_records_json.at(id)["Carbon"].get<float>();
            date_json["Fats"] = date_json.at("Fats").get<float>() - diaries_records_json.at(id)["Fats"].get<float>();
            date_json["Proteins"] = date_json.at("Proteins").get<float>() - diaries_records_json.at(id)["Proteins"].get<float>();

            diaries_records_json.erase(id);

            count = 0;
            type_children children = m_refTreeModel->children();
            for (type_children::iterator iter = children.begin(); iter != children.end(); ++iter)
                (*iter)[m_Columns_Diaries.m_col_id] = count++;

            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (std::round(std::abs((calculData.Calories - date_json.at("Calories").get<float>()) + (initData.Target == ass::TargetLoseWeight ? 300 : initData.Target == ass::TargetSupport ? 0 : 300)) * 10) / 10);
            m_label_need.set_text(resources.need + " " + ((calculData.Calories - date_json.at("Calories").get<float>()) < 0 ? resources.burn : resources.type) + " ~ " + ss.str() + " " + resources.Kkal);

            ss.str("");
            ss << "+ " << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (date_json.at("Calories").get<float>()) << " " << resources.Kkal;
            m_label_nutrition_data.set_text(ss.str());
        }
    }
}

void WinNutrition::show_window_edit_diaries()
{
    // считую старую запись для заполнения полей
    auto idx = (*m_TreeView.get_selection()->get_selected())[m_Columns_Diaries.m_col_id];
    Json old_record = diaries_records_json.at(idx);

    /********** Параметры окна **********/
    window_edit_diaries.set_title(resources.edit_exercise);
    window_edit_diaries.set_position(Gtk::WIN_POS_CENTER);
    window_edit_diaries.set_border_width(BORDER_WIGHT_BOX);
    window_edit_diaries.set_default_size(250, -1);

    /********** Название **********/
    m_entry_name_edit_diaries.set_max_length(20);
    m_frame_name_edit_diaries.set_label_align(0.5, 0.5);
    m_frame_name_edit_diaries.set_label(resources.name_dish);
    m_frame_name_edit_diaries.add(m_entry_name_edit_diaries);
    m_entry_name_edit_diaries.set_text(old_record.value("Name", ""));
    m_VBox_Main_Edit_Diaries.pack_start(m_frame_name_edit_diaries);

    /********** Белки **********/
    ss.str("");
    m_entry_protein_edit_diaries.set_max_length(20);
    m_frame_protein_edit_diaries.set_label_align(0.5, 0.5);
    m_frame_protein_edit_diaries.set_label(resources.protein_in_100g);
    m_frame_protein_edit_diaries.add(m_entry_protein_edit_diaries);
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << old_record.value("Proteins", 0.0f);
    m_entry_protein_edit_diaries.set_text(ss.str());
    m_VBox_Main_Edit_Diaries.pack_start(m_frame_protein_edit_diaries);

    /********** Жиры **********/
    ss.str("");
    m_entry_fat_edit_diaries.set_max_length(20);
    m_frame_fat_edit_diaries.set_label_align(0.5, 0.5);
    m_frame_fat_edit_diaries.set_label(resources.fat_in_100g);
    m_frame_fat_edit_diaries.add(m_entry_fat_edit_diaries);
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << old_record.value("Fats", 0.0f);
    m_entry_fat_edit_diaries.set_text(ss.str());
    m_VBox_Main_Edit_Diaries.pack_start(m_frame_fat_edit_diaries);

    /********** Углеводы **********/
    ss.str("");
    m_entry_carbon_edit_diaries.set_max_length(20);
    m_frame_carbon_edit_diaries.set_label_align(0.5, 0.5);
    m_frame_carbon_edit_diaries.set_label(resources.carbon_in_100g);
    m_frame_carbon_edit_diaries.add(m_entry_carbon_edit_diaries);
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << old_record.value("Carbon", 0.0f);
    m_entry_carbon_edit_diaries.set_text(ss.str());
    m_VBox_Main_Edit_Diaries.pack_start(m_frame_carbon_edit_diaries);

    /********** Калории **********/
    ss.str("");
    m_entry_calories_edit_diaries.set_max_length(20);
    m_frame_calories_edit_diaries.set_label_align(0.5, 0.5);
    m_frame_calories_edit_diaries.set_label(resources.calories_in_100g);
    m_frame_calories_edit_diaries.add(m_entry_calories_edit_diaries);
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << old_record.value("Calories", 0.0f);
    m_entry_calories_edit_diaries.set_text(ss.str());
    m_VBox_Main_Edit_Diaries.pack_start(m_frame_calories_edit_diaries);

    /********** Кнопки **********/
    m_button_cancel_edit_diaries.set_label(resources.cancel);
    m_butonbox_edit_diaries.pack_start(m_button_cancel_edit_diaries);
    m_button_edit_diaries.set_label(resources.save);
    m_butonbox_edit_diaries.pack_start(m_button_edit_diaries);
    m_butonbox_edit_diaries.set_layout(Gtk::BUTTONBOX_EDGE);
    m_VBox_Main_Edit_Diaries.pack_start(m_butonbox_edit_diaries);

    if (isStarted_Edit_Diaries == true)
    {
        m_button_edit_diaries.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_edit_diaries));
        m_button_cancel_edit_diaries.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_cancel_edit_diaries));

        isStarted_Edit_Diaries = false;
    }

    m_VBox_Main_Edit_Diaries.set_spacing(BORDER_WIGHT_BOX);
    window_edit_diaries.add(m_VBox_Main_Edit_Diaries);
    window_edit_diaries.show_all_children();

    Gtk::Main::run(window_edit_diaries);
}

void WinNutrition::on_button_edit_diaries()
{
    auto idx = (*m_TreeView.get_selection()->get_selected())[m_Columns_Diaries.m_col_id];
    Json update_record = diaries_records_json.at(idx);

    float calories((float)std::atoi(m_entry_calories_edit_diaries.get_text().c_str())),
        carbon((float)std::atoi(m_entry_carbon_edit_diaries.get_text().c_str())),
        fats((float)std::atoi(m_entry_fat_edit_diaries.get_text().c_str())),
        proteins((float)std::atoi(m_entry_protein_edit_diaries.get_text().c_str())),
        weigth(100);
    std::string name(m_entry_name_edit_diaries.get_text().c_str());

    // убираю от общей суммы старую запись
    date_json["Calories"] = date_json.value("Calories", 0.0f) - update_record.value("Calories", 0.0f);
    date_json["Carbon"] = date_json.value("Carbon", 0.0f) - update_record.value("Carbon", 0.0f);
    date_json["Fats"] = date_json.value("Fats", 0.0f) - update_record.value("Fats", 0.0f);
    date_json["Proteins"] = date_json.value("Proteins", 0.0f) - update_record.value("Proteins", 0.0f);

    // обновляю поля старой записи
    update_record["Name"] = name;
    // REVIEW: Сделать чтобы пользователь сам выбирал вес
    update_record["Weigth"] = weigth;
    update_record["Calories"] = calories;
    update_record["Carbon"] = carbon;
    update_record["Fats"] = fats;
    update_record["Proteins"] = proteins;

    diaries_records_json[idx] = update_record;

    // прибавляю к общей сумме оюновлёную запись
    date_json["Calories"] = date_json.value("Calories", 0.0f) + calories;
    date_json["Carbon"] = date_json.value("Carbon", 0.0f) + carbon;
    date_json["Fats"] = date_json.value("Fats", 0.0f) + fats;
    date_json["Proteins"] = date_json.value("Proteins", 0.0f) + proteins;

    auto refSelection = m_TreeView.get_selection();
    if (refSelection)
    {
        auto iter = refSelection->get_selected();
        if (iter)
        {
            // [m_Columns_Diaries.m_col_id];
                        ss.str("");
            // row = *(m_refTreeModel->append());
            // row[m_Columns_Diaries.m_col_id] = count++;
            (*iter)[m_Columns_Diaries.m_col_name] = name.c_str();
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << calories;
            (*iter)[m_Columns_Diaries.m_calories] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << carbon;
            (*iter)[m_Columns_Diaries.m_carbon] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << fats;
            (*iter)[m_Columns_Diaries.m_fats] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << proteins;
            (*iter)[m_Columns_Diaries.m_proteins] = ss.str();

        }
    }
    // обновляю запись в таблице
    // auto refSelection = m_TreeView.get_selection();
    // if (refSelection)
    // fill_table();

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (std::round(std::abs((calculData.Calories - date_json.at("Calories").get<float>()) + (initData.Target == ass::TargetLoseWeight ? 300 : initData.Target == ass::TargetSupport ? 0 : 300)) * 10) / 10);
    m_label_need.set_text(resources.need + " " + ((calculData.Calories - date_json.at("Calories").get<float>()) < 0 ? resources.burn : resources.type) + " ~ " + ss.str() + " " + resources.Kkal);

    ss.str("");
    ss << "+ " << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (date_json.at("Calories").get<float>()) << " " << resources.Kkal;
    m_label_nutrition_data.set_text(ss.str());

    WinNutrition::on_button_cancel_edit_diaries();
}

void WinNutrition::on_button_cancel_edit_diaries()
{
    window_edit_diaries.hide();
}

Gtk::ScrolledWindow *WinNutrition::getScrolledWindow()
{
    return &this->m_ScrolledWindow_Main;
}

WinNutrition::~WinNutrition()
{
    if (isRoot == false)
        WinNutrition::save_data();
}

void WinNutrition::fill_struct_calculData()
{
    Json tmp = Json::object();

    // считую даные с json файла
    if (!(ass::FileExists(FILE_USER_STATISTIC_DATA_JSON)))
    {
        system("mkdir -p " FOLDER_USR " " FOLDER_USR_DATA);

        std::ofstream ofs(FILE_USER_STATISTIC_DATA_JSON, std::ios_base::binary);
        ofs << Json::object();
        ofs.close();
    }

    std::ifstream ifs(FILE_USER_STATISTIC_DATA_JSON, std::ios_base::binary);
    ifs >> m_file_statistic_json;
    ifs.close();

    user_json = m_file_statistic_json.value(initData.Name, new_user_json);
    date_json = user_json.value(today_date, new_date_json);
    diaries_records_json = date_json.value("record", Json::array());

    ifs.open(FILE_USER_CALCUL_DATA_JSON, std::ios_base::binary);
    ifs >> tmp;
    ifs.close();

    calculData.Water = tmp[initData.Name]["water"];
    calculData.Calories = tmp[initData.Name]["calories"];
    calculData.Fats = tmp[initData.Name]["fats"];
    calculData.Carbon = tmp[initData.Name]["carbon"];
    calculData.Proteins = tmp[initData.Name]["proteins"];

    m_label_BMR_data.set_text(std::to_string(calculData.Calories) + " " + resources.Kkal);

    ss.str("");
    ss << "+ " << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (date_json.at("Calories").get<float>()) << " " << resources.Kkal;
    m_label_nutrition_data.set_text(ss.str());

    ss.str("");
    ss << (std::round(std::abs(calculData.Calories - date_json.at("Calories").get<float>()) * 10) / 10);
    m_label_target_data.set_text((initData.Target == ass::TargetLoseWeight ? "- 300 " + resources.Kkal : (initData.Target == ass::TargetSupport ? "+ 0 " + resources.Kkal : "+ 300 " + resources.Kkal)));

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (std::round(std::abs((calculData.Calories - date_json.at("Calories").get<float>()) + (initData.Target == ass::TargetLoseWeight ? 300 : initData.Target == ass::TargetSupport ? 0 : 300)) * 10) / 10);
    m_label_need.set_text(resources.need + " " + ((calculData.Calories - date_json.at("Calories").get<float>()) < 0 ? resources.burn : resources.type) + " ~ " + ss.str() + " " + resources.Kkal);
}

void WinNutrition::fill_table()
{
    for (auto &var : diaries_records_json)
    {
        ss.str("");
        row = *(m_refTreeModel->append());
        row[m_Columns_Diaries.m_col_id] = count++;
        row[m_Columns_Diaries.m_col_name] = var["Name"].get<std::string>().c_str();
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Calories").get<float>();
        row[m_Columns_Diaries.m_calories] = ss.str();
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Carbon").get<float>();
        row[m_Columns_Diaries.m_carbon] = ss.str();
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Fats").get<float>();
        row[m_Columns_Diaries.m_fats] = ss.str();
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Proteins").get<float>();
        row[m_Columns_Diaries.m_proteins] = ss.str();
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Weigth").get<float>();
        row[m_Columns_Diaries.m_weigth] = ss.str();
    }
}

void WinNutrition::set_list_user()
{
    ass::InitData *temp(new ass::InitData);

    size_t size(sizeof(ass::InitData));
    auto completion(Gtk::EntryCompletion::create());
    m_entry_find.set_completion(completion);

    auto refCompletionModel(Gtk::ListStore::create(m_Columns));
    completion->set_model(refCompletionModel);

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);

    Gtk::TreeModel::Row row = *(refCompletionModel->append());
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);

    while (ifs.read((char *)temp, size))
    {
        row = *(refCompletionModel->append());
        row[m_Columns.m_col_name] = temp->Name;
    }

    ifs.close();

    completion->set_text_column(m_Columns.m_col_name);

    delete temp;
}

void WinNutrition::on_button_enter_find()
{
    find_user();
}

void WinNutrition::on_button_icon_find(Gtk::EntryIconPosition, const GdkEventButton *)
{
    find_user();
}

void WinNutrition::find_user()
{
    if (strcmp(initData.Name, m_entry_find.get_text().c_str()) == 0)
        return;

    ass::InitData *temp = new ass::InitData;
    size_t size = sizeof(ass::InitData),
           index(0);

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);

    while (ifs.read((char *)temp, size))
        if (strcmp(temp->Name, m_entry_find.get_text().c_str()) == 0)
        {
            nUserInit = index;
            break;
        }
        else
            index++;

    ifs.close();

    m_label_find.set_text((nUserInit == -1 ? resources.user_not_found + "!" : resources.find_user));
    m_label_find.override_color(Gdk::RGBA((nUserInit == -1 ? "yellow" : "white")), Gtk::STATE_FLAG_NORMAL);

    if (nUserInit != -1)
    {
        fill_struct_initData();
        fill_struct_calculData(); // заполняю все поля

        ss.str("");
        need_water.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << date_json.at("Water").get<float>();
        need_water << std::setprecision(2) << (calculData.Water / 1000.0);
        m_label_result_water.set_text(ss.str() + "/" + need_water.str() + " " + resources.litr + ".");

        // Очистка таблицы
        type_children children = m_refTreeModel->children();
        for (Gtk::TreeModel::Children::iterator iter = children.begin(); iter != children.end(); iter++)
            m_refTreeModel->erase(iter); //in a while loop

        // Заполнение таблицы новыми даными
        fill_table();
    }

    delete temp;
}

void WinNutrition::fill_struct_initData()
{
    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);

    if (ifs.is_open())
    {
        ifs.seekg(nUserInit * sizeof(ass::InitData) + sizeof(ass::root), std::ios_base::beg);
        ifs.read((char *)&initData, sizeof(initData));
        ifs.close();
    }
}

void WinNutrition::save_data()
{
    /* Запись изменений в файл */
    date_json["record"] = diaries_records_json;
    user_json[today_date] = date_json;
    m_file_statistic_json[initData.Name] = user_json;

    std::ofstream ofs(FILE_USER_STATISTIC_DATA_JSON, std::ios_base::binary);
    ofs << m_file_statistic_json.dump(SPACE_IN_JSON_FILES);
    ofs.close();
}

void WinNutrition::on_button_add_1()
{
    date_json.at("Water") = date_json.value("Water", 0.0f) + 1.0f;

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << date_json.at("Water").get<float>();
    m_label_result_water.set_text(ss.str() + "/" + need_water.str() + " л.");
}

void WinNutrition::on_button_add_0_5()
{
    date_json.at("Water") = date_json.value("Water", 0.0f) + 0.5f;

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << date_json.at("Water").get<float>();
    m_label_result_water.set_text(ss.str() + "/" + need_water.str() + " л.");
}

void WinNutrition::on_button_add_0_2()
{
    date_json.at("Water") = date_json.value("Water", 0.0f) + 0.2f;

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << date_json.at("Water").get<float>();
    m_label_result_water.set_text(ss.str() + "/" + need_water.str() + " л.");
}

void WinNutrition::on_button_minus_1()
{
    date_json.at("Water") = ((date_json["Water"].get<float>() - 1.0f) < 0.0f
                                 ? 0.0f
                                 : date_json.value("Water", 0.0f) - 1.0f);

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << date_json.at("Water").get<float>();
    m_label_result_water.set_text(ss.str() + "/" + need_water.str() + " л.");
}

void WinNutrition::on_button_minus_0_5()
{
    date_json.at("Water") = ((date_json["Water"].get<float>() - 0.5) < 0.0f
                                 ? 0.0f
                                 : date_json.value("Water", 0.0f) - 0.5f);

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << date_json.at("Water").get<float>();
    m_label_result_water.set_text(ss.str() + "/" + need_water.str() + " л.");
}

void WinNutrition::on_button_minus_0_2()
{
    date_json.at("Water") = ((date_json["Water"].get<float>() - 0.2f) < 0.0f
                                 ? 0.0f
                                 : date_json.value("Water", 0.0f) - 0.2f);

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << date_json.at("Water").get<float>();
    m_label_result_water.set_text(ss.str() + "/" + need_water.str() + " л.");
}

void WinNutrition::show_window_add_diaries()
{
    /********** Параметры окна **********/
    window.set_title(resources.title_diaries_addeding);
    window.set_position(Gtk::WIN_POS_CENTER);
    window.set_border_width(BORDER_WIGHT_BOX);
    window.set_default_size(250, -1);

    /********** Название **********/
    m_entry_name.set_max_length(20);
    m_frame_name.set_label_align(0.5, 0.5);
    m_frame_name.add(m_entry_name);
    m_entry_name.set_text("");
    m_VBox_Main_Diaries.pack_start(m_frame_name);

    /********** Белки **********/
    m_entry_protein.set_max_length(20);
    m_frame_protein.set_label_align(0.5, 0.5);
    m_frame_protein.add(m_entry_protein);
    m_entry_protein.set_text("");
    m_VBox_Main_Diaries.pack_start(m_frame_protein);

    /********** Жиры **********/
    m_entry_fat.set_max_length(20);
    m_frame_fat.set_label_align(0.5, 0.5);
    m_frame_fat.add(m_entry_fat);
    m_entry_fat.set_text("");
    m_VBox_Main_Diaries.pack_start(m_frame_fat);

    /********** Углеводы **********/
    m_entry_carbon.set_max_length(20);
    m_frame_carbon.set_label_align(0.5, 0.5);
    m_frame_carbon.add(m_entry_carbon);
    m_entry_carbon.set_text("");
    m_VBox_Main_Diaries.pack_start(m_frame_carbon);

    /********** Калории **********/
    m_entry_calories.set_max_length(20);
    m_frame_calories.set_label_align(0.5, 0.5);
    m_frame_calories.add(m_entry_calories);
    m_entry_calories.set_text("");
    m_VBox_Main_Diaries.pack_start(m_frame_calories);

    /********** Кнопки **********/
    m_butonbox_diaries.pack_start(m_button_cancel_diaries);
    m_butonbox_diaries.pack_start(m_button_add_diaries);
    m_butonbox_diaries.set_layout(Gtk::BUTTONBOX_EDGE);
    m_VBox_Main_Diaries.pack_start(m_butonbox_diaries);

    if (isStarted == true)
    {
        m_button_add_diaries.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_add_diaries));
        m_button_cancel_diaries.signal_clicked().connect(sigc::mem_fun(*this, &WinNutrition::on_button_cancel_diaries));

        isStarted = false;
    }

    m_VBox_Main_Diaries.set_spacing(BORDER_WIGHT_BOX);
    window.add(m_VBox_Main_Diaries);
    window.show_all_children();

    Gtk::Main::run(window);
}

void WinNutrition::on_button_add_diaries()
{
    Json new_record = Json::object();
    std::string name(m_entry_name.get_text().c_str());

    float calories((float)std::atoi(m_entry_calories.get_text().c_str())),
        carbon((float)std::atoi(m_entry_carbon.get_text().c_str())),
        fats((float)std::atoi(m_entry_fat.get_text().c_str())),
        proteins((float)std::atoi(m_entry_protein.get_text().c_str())),
        weigth(100);

    new_record["Name"] = name;
    // REVIEW: Сделать чтобы пользователь сам выбирал вес
    new_record["Weigth"] = weigth;
    new_record["Calories"] = calories;
    new_record["Fats"] = fats;
    new_record["Carbon"] = carbon;
    new_record["Proteins"] = proteins;

    diaries_records_json.push_back(new_record);

    date_json["Calories"] = date_json.value("Calories", 0.0f) + calories;
    date_json["Carbon"] = date_json.value("Carbon", 0.0f) + carbon;
    date_json["Fats"] = date_json.value("Fats", 0.0f) + fats;
    date_json["Proteins"] = date_json.value("Proteins", 0.0f) + proteins;

    row = *(m_refTreeModel->append());
    row[m_Columns_Diaries.m_col_id] = count++;
    row[m_Columns_Diaries.m_col_name] = name;
    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << calories;
    row[m_Columns_Diaries.m_calories] = ss.str();
    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << carbon;
    row[m_Columns_Diaries.m_carbon] = ss.str();
    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << fats;
    row[m_Columns_Diaries.m_fats] = ss.str();
    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << proteins;
    row[m_Columns_Diaries.m_proteins] = ss.str();
    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << weigth;
    row[m_Columns_Diaries.m_weigth] = ss.str();

    // Реалиищовать обновление
    ss.str("");
    ss << "+ " << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (date_json.at("Calories").get<float>()) << " " << resources.Kkal;
    m_label_nutrition_data.set_text(ss.str());

    ss.str("");
    ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << (std::round(std::abs((calculData.Calories - date_json.at("Calories").get<float>()) + (initData.Target == ass::TargetLoseWeight ? 300 : initData.Target == ass::TargetSupport ? 0 : 300)) * 10) / 10);
    m_label_need.set_text(resources.need + " " + ((calculData.Calories - date_json.at("Calories").get<float>()) < 0 ? resources.burn : resources.type) + " ~ " + ss.str() + " " + resources.Kkal);

    WinNutrition::on_button_cancel_diaries();
}

void WinNutrition::on_button_cancel_diaries()
{
    window.hide();
}

const std::string WinNutrition::get_date(const char *format)
{
    time_t now = time(0);
    struct tm tstruct = *localtime(&now);
    char buf[80];

    strftime(buf, sizeof(buf), format, &tstruct);

    return buf;
}