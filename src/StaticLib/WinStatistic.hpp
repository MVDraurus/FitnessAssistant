// -*- С++ -*-
#ifndef GTKMM_STATISTIC_HPP
#define GTKMM_STATISTIC_HPP

#include "FitnessAssistant.hpp"
#include <gtkmm.h>
#include <iomanip>

class WinStatistic : public Gtk::ScrolledWindow
{
  public:
	WinStatistic(int nUser, bool isRood);

	Gtk::ScrolledWindow *getScrolledWindow();

  private:
	typedef Gtk::TreeModel::Children type_children;

	/* Шаблон таблицы пользователей */
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	  public:
		ModelColumns()
		{
			add(m_col_data);
			add(m_col_calorie);
			add(m_col_fat);
			add(m_col_carbon);
			add(m_col_protein);
			add(m_col_water);
			add(m_col_name);
		}

		Gtk::TreeModelColumn<Glib::ustring> m_col_data;
		Gtk::TreeModelColumn<Glib::ustring> m_col_calorie;
		Gtk::TreeModelColumn<Glib::ustring> m_col_fat;
		Gtk::TreeModelColumn<Glib::ustring> m_col_carbon;
		Gtk::TreeModelColumn<Glib::ustring> m_col_protein;
		Gtk::TreeModelColumn<Glib::ustring> m_col_water;
		Gtk::TreeModelColumn<Glib::ustring> m_col_name;
	};

	/* Реализует вывод подсказки, в поле ввода имени пользователя */
	class ModelColumnsPreview : public Gtk::TreeModel::ColumnRecord
	{
	  public:
		ModelColumnsPreview()
		{
			add(m_col_name);
		}

		Gtk::TreeModelColumn<Glib::ustring> m_col_name;
	} m_Columns_Preview;

	ModelColumns *m_Columns = new ModelColumns;

	// TODO: Задокументировать
	void on_button_icon_find(Gtk::EntryIconPosition, const GdkEventButton *);
	// TODO: Задокументировать
	void user_is_not_found();
	// TODO: Задокументировать
	void read_statistic();
	// TODO: Задокументировать
	void fill_table(std::string name);
	// TODO: Задокументировать
	void statistic_is_not_found();
	// TODO: Задокументировать
	void on_button_enter_find();
	// TODO: Задокументировать
	void find_user();

	/***
     * Функция set_list_user(), считывает имена всех зарезервированых 
     * пользователей из файла FILE_USER_INIT_DATA и добавляет их в подсказку,
     * которая будет отображаться при поиске.
     */
	void set_list_user();

	bool isRoot;
	int nUser;

	Json m_file_statistic_json, date_json, diaries_records_json, user_json;
	std::stringstream ss;
	std::vector<ass::Statistic> statistic;
	ass::InitData initData;

	Gtk::TreeView m_TreeView;
	Gtk::TreeModel::Row row, childrow;
	Glib::RefPtr<Gtk::TreeStore> m_refTreeModel;
	Gtk::ScrolledWindow m_ScrolledWindow;
	Gtk::Label m_label_find;
	Gtk::Entry m_entry_find;
	Gtk::VBox m_VBox_Main, m_VBox_Find;
	Gtk::HBox m_HBox_Find;
};

#endif // GTKMM_STATISTIC_HPP