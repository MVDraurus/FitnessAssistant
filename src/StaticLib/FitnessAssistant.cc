#include "FitnessAssistant.hpp"

bool ass::FileEmpty(std::string FileName)
{
    return std::ifstream(FileName, std::ios_base::binary | std::ios_base::ate).tellg() == 0;
}

bool ass::FileExists(std::string FileName)
{
    return std::ifstream(FileName, std::ios_base::in).is_open();
}

ass::Note::Note(std::string name, int weigth, int calories, int protein,
                int fat, int carbon)
    : m_label_name(name),
      m_label_weigth(std::to_string(weigth) + "г"),
      m_label_calories(" " + std::to_string(calories) + " "),
      m_label_protein(" " + std::to_string(protein) + " "),
      m_label_fat(" " + std::to_string(fat) + " "),
      m_label_carbon(" " + std::to_string(carbon) + " ")
{
    this->set_spacing(10);

    /********** Имя продукта *********/
    m_label_name.set_halign(Gtk::ALIGN_START);

    this->pack_start(m_label_name, Gtk::PACK_SHRINK);

    /********** вес *********/
    m_label_weigth.set_halign(Gtk::ALIGN_START);
    // m_label_weigth.override_color(Gdk::RGBA("red"), Gtk::STATE_FLAG_NORMAL);
    // m_label_calories.override_background_color(Gdk::RGBA("blue"), Gtk::STATE_FLAG_NORMAL);

    this->pack_start(m_label_weigth, Gtk::PACK_SHRINK);

    /********** калории *********/
    m_label_calories.set_halign(Gtk::ALIGN_END);
    m_label_calories.override_background_color(Gdk::RGBA("grey"), Gtk::STATE_FLAG_NORMAL);

    this->pack_start(m_label_calories);

    /********** протеины *********/
    m_label_protein.set_halign(Gtk::ALIGN_END);
    m_label_protein.override_background_color(Gdk::RGBA("orange"), Gtk::STATE_FLAG_NORMAL);

    this->pack_start(m_label_protein, Gtk::PACK_SHRINK);

    /********** жиры *********/
    m_label_fat.set_halign(Gtk::ALIGN_END);
    m_label_fat.override_background_color(Gdk::RGBA("green"), Gtk::STATE_FLAG_NORMAL);

    this->pack_start(m_label_fat, Gtk::PACK_SHRINK);

    /********** углеводы *********/
    m_label_carbon.set_halign(Gtk::ALIGN_END);
    m_label_carbon.override_background_color(Gdk::RGBA("purple"), Gtk::STATE_FLAG_NORMAL);

    this->pack_start(m_label_carbon, Gtk::PACK_SHRINK);
}

ass::InitData ass::getInitData(size_t nUser)
{
    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);
    ass::InitData initData;

    if (ifs.is_open())
    {
        ifs.seekg(nUser * sizeof(ass::InitData) + sizeof(ass::root), std::ios_base::beg);
        ifs.read((char *)&initData, sizeof(initData));
        ifs.close();
    }

    return initData;
}

ass::InitData ass::getInitDataFromName(std::string name)
{
    ass::InitData initData;

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);
    ifs.seekg(sizeof(ass::root), std::ios_base::beg);

    while (ifs.read((char *)&initData, sizeof(initData)))
        if (strcmp(initData.Name, name.c_str()) == 0)
            break;
        
    ifs.close();

    return initData;
}
