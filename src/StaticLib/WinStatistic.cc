#include "WinStatistic.hpp"

WinStatistic::WinStatistic(int nUser, bool isRoot)
    : nUser(nUser), isRoot(isRoot),
      m_label_find(resources.find_user),
      m_file_statistic_json(Json::object()),
      date_json(Json::object()),
      diaries_records_json(Json::object()),
      user_json(Json::object())
{
    read_statistic(); // читаю статистику
    set_list_user();
    // VISIT: https://developer.gnome.org/gtkmm-tutorial/stable/sec-treeview-examples.html.en
    if (isRoot == true)
    {
        // строка поиска для админа
        m_VBox_Find.pack_start(m_label_find);
        m_entry_find.set_icon_from_icon_name("edit-find", Gtk::ENTRY_ICON_SECONDARY);

        m_entry_find.signal_icon_press().connect(sigc::mem_fun(*this, &WinStatistic::on_button_icon_find));
        m_entry_find.signal_activate().connect(sigc::mem_fun(*this, &WinStatistic::on_button_enter_find));

        m_VBox_Find.pack_start(m_entry_find);
        m_HBox_Find.pack_start(m_VBox_Find);

        m_VBox_Find.set_border_width(5);
        m_VBox_Main.pack_start(m_HBox_Find, Gtk::PACK_SHRINK);
    }
    else
    {
        initData = ass::getInitData(nUser);
        // ss << resources.statistic << "-" << (isRoot == true ? resources.statistic + "-" + resources.admin : initData.Name);
    }

    m_VBox_Main.set_border_width(10);

    /********** Окно просмотра статистики **********/
    m_refTreeModel = Gtk::TreeStore::create(*m_Columns);
    m_TreeView.set_model(m_refTreeModel);

    m_TreeView.append_column(resources.date, m_Columns->m_col_data);
    m_TreeView.append_column(resources.calories, m_Columns->m_col_calorie);
    m_TreeView.append_column(resources.fats, m_Columns->m_col_fat);
    m_TreeView.append_column(resources.carbons, m_Columns->m_col_carbon);
    m_TreeView.append_column(resources.proteins, m_Columns->m_col_protein);
    m_TreeView.append_column(resources.water, m_Columns->m_col_water);

    m_ScrolledWindow.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
    m_ScrolledWindow.add(m_TreeView);

    if (isRoot == false)
        fill_table(initData.Name);

    m_VBox_Main.pack_start(m_ScrolledWindow);

    add(m_VBox_Main);
}

Gtk::ScrolledWindow *WinStatistic::getScrolledWindow()
{
    return this;
}

void WinStatistic::set_list_user()
{
    ass::InitData *temp = new ass::InitData;
    ass::root *root = new ass::root;
    size_t size = sizeof(ass::InitData);
    auto completion = Gtk::EntryCompletion::create();

    auto refCompletionModel = Gtk::ListStore::create(m_Columns_Preview);
    completion->set_model(refCompletionModel);

    m_entry_find.set_completion(completion);

    if (!(ass::FileExists(FILE_USER_INIT_DATA)))
    {
        std::ofstream ofs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::out);
        ofs.write((char *)root, sizeof(ass::root));
        ofs.close();
    }

    std::ifstream ifs(FILE_USER_INIT_DATA, std::ios_base::binary | std::ios_base::in);

    Gtk::TreeModel::Row row_preview = *(refCompletionModel->append());
    ifs.read((char *)root, sizeof(ass::root));
    row_preview[m_Columns_Preview.m_col_name] = root->name;

    while (ifs.read((char *)temp, size))
    {
        row_preview = *(refCompletionModel->append());
        row_preview[m_Columns_Preview.m_col_name] = temp->Name;
    }

    ifs.close();

    completion->set_text_column(m_Columns_Preview.m_col_name);

    delete temp;
    delete root;
}

void WinStatistic::on_button_enter_find()
{
    find_user();
}

void WinStatistic::on_button_icon_find(Gtk::EntryIconPosition, const GdkEventButton *)
{
    find_user();
}

void WinStatistic::find_user()
{
    std::string userName = m_entry_find.get_text().c_str();
    if (userName.empty() || userName.compare(initData.Name) == 0)
        return;

    initData = ass::getInitDataFromName(userName);
    fill_table(initData.Name);
}

void WinStatistic::read_statistic()
{
    // считую даные с json файла
    if (!(ass::FileExists(FILE_USER_STATISTIC_DATA_JSON)))
    {
        system("mkdir -p " FOLDER_USR " " FOLDER_USR_DATA);

        std::ofstream ofs(FILE_USER_STATISTIC_DATA_JSON, std::ios_base::binary);
        ofs << Json::object();
        ofs.close();
    }

    std::ifstream ifs(FILE_USER_STATISTIC_DATA_JSON, std::ios_base::binary);
    ifs >> m_file_statistic_json;
    ifs.close();

    // user_json = m_file_statistic_json.value(name, new_user_json);
    // std::cout << m_file_statistic_json.dump(SPACE_IN_JSON_FILES) << std::endl;
    // date_json = user_json.value(today_date, new_date_json);
    // diaries_records_json = date_json.value("record", Json::array());

    // ass::Statistic _;

    // std::ifstream ifs(FILE_USER_STATISTIC_DATA, std::ios_base::binary | std::ios_base::in);
    // statistic.clear();

    // while (ifs.read((char *)&_.Date, sizeof(_.Date)) &&
    //        ifs.read((char *)&_.Name, sizeof(_.Name)) &&
    //        ifs.read((char *)&_.Water, sizeof(_.Water)) &&
    //        ifs.read((char *)&_.Calories, sizeof(_.Calories)) &&
    //        ifs.read((char *)&_.Fats, sizeof(_.Fats)) &&
    //        ifs.read((char *)&_.Carbon, sizeof(_.Carbon)) &&
    //        ifs.read((char *)&_.Proteins, sizeof(_.Proteins)) &&
    //        ifs.read((char *)&_.count, sizeof(_.count)) &&
    //        ifs.read((char *)_.record, sizeof(_.record[0]) * 20))
    // {
    //     if ((strcmp(_.Name, name.c_str()) == 0))
    //         statistic.push_back(_);
    // }

    // if (statistic.empty() && isRoot == true)
    //     user_is_not_found();
    // else if (statistic.empty() && isRoot == false)
    //     statistic_is_not_found();
    // else
    // {
    //     fill_table();
    // }

    // ifs.close();
}

void WinStatistic::user_is_not_found()
{
    Gtk::MessageDialog m_MessageDialog(resources.UPS);
    m_MessageDialog.set_secondary_text(resources.user_with_name_not_found);
    m_MessageDialog.run();
}

void WinStatistic::statistic_is_not_found()
{
    Gtk::MessageDialog m_MessageDialog(resources.UPS);
    m_MessageDialog.set_secondary_text(resources.you_not_have_statistic);
    m_MessageDialog.run();
    // hide();
}

void WinStatistic::fill_table(std::string name)
{
    assert(!name.empty());
    user_json = m_file_statistic_json.value(name, Json::object());

    // Очистка таблицы
    m_refTreeModel->clear();

    for (auto &dates : user_json.items())
    {
        row = *(m_refTreeModel->append());
        row[m_Columns->m_col_data] = dates.key();
        diaries_records_json = dates.value().at("record");

        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << dates.value().at("Calories").get<float>();
        row[m_Columns->m_col_calorie] = ss.str();
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << dates.value().at("Fats").get<float>();
        row[m_Columns->m_col_fat] = ss.str();
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << dates.value().at("Carbon").get<float>();
        row[m_Columns->m_col_carbon] = ss.str();
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << dates.value().at("Proteins").get<float>();
        row[m_Columns->m_col_protein] = ss.str();
        ss.str("");
        ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << dates.value().at("Water").get<float>();
        row[m_Columns->m_col_water] = ss.str();

        for (auto &record : diaries_records_json)
        {
            childrow = *(m_refTreeModel->append(row.children()));

            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << record["Calories"].get<float>();
            childrow[m_Columns->m_col_calorie] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << record["Fats"].get<float>();
            childrow[m_Columns->m_col_fat] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << record["Carbon"].get<float>();
            childrow[m_Columns->m_col_carbon] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << record["Proteins"].get<float>();
            childrow[m_Columns->m_col_protein] = ss.str();
            ss.str("");
            ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << record["Name"].get<std::string>();
            childrow[m_Columns->m_col_data] = ss.str();

            // ss.str("");
            // ss << std::fixed << std::setprecision(NUMBER_OF_ZEROS_AFTER_COMMA) << var.at("Weigth").get<float>();
            // row[m_Columns_Diaries.m_weigth] = ss.str();
        }

        diaries_records_json.clear();
    }
}